/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   other_tools.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/27 22:25:03 by algrele           #+#    #+#             */
/*   Updated: 2018/05/19 03:11:50 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include <pf_mini_libft.h>

int		c_in_macro(char c, char *macro_name)
{
	int i;

	i = 0;
	while (macro_name[i])
	{
		if (c == macro_name[i])
			return (1);
		i++;
	}
	return (0);
}

int		i_in_macro(char c, char *macro_name)
{
	int i;

	i = 0;
	while (macro_name[i])
	{
		if (c == macro_name[i])
			return (i);
		i++;
	}
	return (-1);
}
