/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/28 03:25:16 by algrele           #+#    #+#             */
/*   Updated: 2019/03/17 22:20:05 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include <pf_mini_libft.h>
#include <printf_tests.h>

/*
** create, use and free t_format struct : for a given set of instructions,
** if conversion is supported, call apply_type to show result. return -1 if
** print_ functions from apply type, or malloc of struct failed.
** if conversion is not supported, apply instructions to it as if it were
** a simple character.
*/

static int	get_next_instructions(char *format, va_list ap, int *skip)
{
	t_format	*b;
	int			ret;

	ret = 0;
	if (!(b = init_format_struct()))
		return (-1);
	b = parse_instructions(b, format, skip);
	if (c_in_macro(b->type, CONVERSIONS))
		ret = apply_type(b, ap);
	else if (format[1])
		ret = apply_char_options(b, b->type);
	free(b);
	return (ret);
}

static int	get_next_unicode(char *format, va_list ap, int *skip)
{
	t_format	*b;
	int			ret;
	int			start;
	wint_t		argument;

	if (!(b = init_format_struct()))
		return (-1);
	start = *skip;
	*skip = 0;
	b = parse_instructions(b, format + start, skip);
	argument = va_arg(ap, wint_t);
	if (!check_error_wide_c(argument))
		return (-1);
	write(1, &format[0], start);
	ret = print_wchar(b, argument);
	*skip = start + *skip;
	free(b);
	return (start + ret);
}

/*
** write plain text strings with a single system call to write. adds colors.
** checks for -1 returns with unicode.
*/

static int	print_from_format(const char *format, int *s, va_list ap)
{
	int	len;

	len = 0;
	while (format[*s] && format[*s] != '%')
	{
		if (B_COLOR && format[*s] == '{' && format[*s + 1] == 'c'
				&& format[*s + 2] == '}')
		{
			write(1, &format[0], *s);
			pf_print_color(s, ap);
			return (*s - 3);
		}
		else if (B_COLOR && format[*s] == '{' && format[*s + 1] == 'r'
				&& format[*s + 2] == 'c' && format[*s + 3] == '}')
		{
			write(1, EOC, 4);
			return (*s - 4);
		}
		(*s)++;
	}
	if (!is_next_unicode_conv(format))
		write(1, &format[0], *s);
	else if ((len = get_next_unicode((char *)format, ap, s)) == -1)
		*s = -1;
	return (len > 0 ? len : *s);
}

/*
** go through format string : print plain text (print_from_format)
** when a '%' is found, call get_next_instructions to parse instructions and
** print result. if a get_next_instructions failed, ft_printf returns -1.
** otherwise, return number of characters (bytes to be more exact) written.
*/

int			ft_printf(const char *format, ...)
{
	va_list		ap;
	int			i;
	int			len;

	len = 0;
	i = 0;
	va_start(ap, format);
	while (format && format[0] && i >= 0 && len >= 0)
	{
		i = 0;
		if (format[0] != '%')
			len = len + print_from_format(format, &i, ap);
		else if (format[0] == '%')
			len = len + get_next_instructions((char *)format, ap, &i);
		format = format + i;
	}
	if (B_COLOR)
		write(1, EOC, 4);
	va_end(ap);
	return (i < 0 ? -1 : len);
}
