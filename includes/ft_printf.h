/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/19 02:52:52 by algrele           #+#    #+#             */
/*   Updated: 2019/02/18 16:54:14 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <unistd.h>
# include <wchar.h>
# include <inttypes.h>

typedef struct		s_format
{
	char			flags[5];
	char			type;
	int				field;
	int				preci;
	int				modi;
}					t_format;

/*
** BONUS : todo : binary, fd, non printables, convert base, etc...
*/

# define B_COLOR 0

/*
** CONVERSION, FLAG AMD MODIFIER MACROS
*/

# define CONVERSIONS "sSpdDioOuUxXcC%"
# define U_CONVERSIONS "oOuUxX"
# define NP_CONVERSIONS "idDoOuUxXp"
# define NUMBERS "0123456789"

# define FLAGS "#0-+ "
# define F_SHARP 0
# define F_ZERO 1
# define F_MINUS 2
# define F_PLUS 3
# define F_SPACE 4

# define MODIFIERS "hljz"
# define M_H 1
# define M_HH 2
# define M_L 3
# define M_LL 4
# define M_J 5
# define M_Z 6

/*
** UNICODE MACROS
*/

# define MASK1 "0xxxxxxx"
# define MASK2 "110xxxxx"
# define MASK3 "1110xxxx"
# define MASK4 "11110xxx"
# define MASKF "10xxxxxx"

/*
**	FT_PRINTF -> get_next_instructions and apply type loop
*/
int					ft_printf(const char *format, ...);

/*
**	INSTRUCTIONS_PARSER -> fill t_format struct with instructions
*/
t_format			*parse_instructions(t_format *b, char *format, int *skip);

/*
**	PARSER_TOOLS
*/
int					clean_type(t_format *bubble);
int					clean_flags(t_format *bubble);
void				check_cut_flags(t_format *bubble, char *flags, int stop);
void				assign_modifier(char first, char second, int *modifier);
t_format			*init_format_struct(void);

/*
** APPLY_TYPE -> retrieve va_list argument and send to print_ functions
*/
int					apply_type(t_format *bubble, va_list arg);

/*
** PRINT_NUMBERS -> call ntoa_base and apply_num_options
*/
int					print_number(t_format *bubble, intmax_t argument);
int					print_pointer(t_format *b, void *argument);

/*
** PRINT_CHARS_STRINGS
*/
int					print_char(t_format *b, char argument);
int					print_wchar(t_format *b, wint_t argument);
int					print_string(t_format *b, char *argument);
int					print_wstring(t_format *b, wchar_t *argument);
int					print_null(t_format *b);

/*
**	NTOA_BASE -> number to base, signed and unsigned, with max int sizes
*/
char				*ft_untoa_base(uintmax_t decimal, int base, int maj);
char				*ft_ntoa_base(intmax_t decimal, int base);

/*
** NUMBER_OPTIONS -> apply precision, flags, field length to number conversion
*/
int					apply_num_options(t_format *b, char *number);

/*
** PRINT_UNICODE1
*/
int					ft_putwchar(wint_t c, int max_bytes);
int					ft_putwstr(wchar_t *s, int max_bytes);

/*
** PRINT_UNICODE2
*/
int					is_next_unicode_conv(const char *format);
int					check_error_wide_c(wint_t wc);
int					count_wchar_bytes(wint_t wc);
int					count_max_bytes(wchar_t *s, int toggle, int max_bytes);

/*
**	JOIN_TOOLS
*/
char				*ft_join_head(char *suffix, char *src);
char				*ft_join_n_head(char prefix, char *src, int n);
char				*ft_join_n_tail(char suffix, char *src, int n);
char				*ft_join_n_at(char addme, char *src, int start, int n);

/*
** CHAR_OPTIONS
*/
void				print_extra_spaces(int field, char c, int pos, char extra);
int					apply_char_options(t_format *b, char c);
int					apply_wchar_options(t_format *b, wint_t wc);

/*
** STRING_OPTIONS
*/
int					apply_string_options(t_format *b, char *str);
int					apply_wstring_options(t_format *b, wchar_t *argument);

/*
**	OTHER_TOOLS
*/
int					c_in_macro(char c, char *macro_name);
int					i_in_macro(char c, char *macro_name);

#endif
