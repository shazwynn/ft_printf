/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pf_mini_libft.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/03 17:44:22 by algrele           #+#    #+#             */
/*   Updated: 2018/05/19 03:26:59 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PF_MINI_LIBFT_H
# define PF_MINI_LIBFT_H

# include <unistd.h>
# include <stdlib.h>
# include <stdarg.h>

/*
** COLOR MACROS
*/

# define EOC "\e[0m"
# define P_RED "\e[38;5;13m"
# define RED "\e[38;5;196m"
# define P_PINK "\e[38;5;204m"
# define PINK "\e[38;5;198m"
# define P_PURPLE "\e[38;5;135m"
# define PURPLE "\e[38;5;128m"
# define P_BLUE "\e[38;5;39m"
# define BLUE "\e[38;5;32m"
# define TURQUOISE "\e[38;5;36m"
# define P_GREEN "\e[38;5;119m"
# define GREEN "\e[38;5;119m"
# define YELLOW "\e[38;5;226m"
# define SUN "\e[38;5;208m"
# define ORANGE "\e[38;5;202m"

/*
** PUT_FUNCTIONS
*/

int		pf_putchar(char c);
void	pf_putstr(char *str);
void	pf_putnbr(int n);

/*
** PRINTF_COLOR_TOOLS
*/

void	pf_putchar_c(char c, char *color);
void	pf_putstr_c(char *s, char *color);
void	pf_putendl_c(char *s, char *color);
void	pf_putnbr_c(int n, char *color);
void	pf_print_color(int *skip, va_list ap);

/*
** STR
*/

size_t	pf_strlen(const char *s);
char	*pf_strcpy(char *dst, const char *src);
char	*pf_strncpy(char *dst, const char *src, size_t n);

/*
** MEM STR
*/

void	pf_bzero(void *s, size_t n);
char	*pf_strnew(size_t size);
void	pf_strdel(char **as);
char	*pf_strdup(const char *s1);

/*
** MEM
*/

void	*pf_memset(void *b, int c, size_t len);

/*
** CALC - CONV
*/

int		pf_atoi(char *str);
int		pf_atoi_base(char *str, char *base);

#endif
