# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: algrele <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/03 22:30:52 by algrele           #+#    #+#              #
#    Updated: 2019/03/17 22:30:26 by algrele          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a

SRC = ft_printf.c \
	  print_numbers.c \
	  print_chars_strings.c \
	  print_unicode1.c \
	  print_unicode2.c \
	  apply_type.c \
	  number_options.c \
	  instructions_parser.c \
	  ntoa_base.c \
	  char_options.c \
	  string_options.c \
	  parser_tools.c \
	  join_tools.c \
	  other_tools.c \
	  srcs_libft/pf_strlen.c \
	  srcs_libft/pf_strcpy.c \
	  srcs_libft/pf_strncpy.c \
	  srcs_libft/pf_atoi.c \
	  srcs_libft/pf_bzero.c \
	  srcs_libft/pf_strnew.c \
	  srcs_libft/pf_strdel.c \
	  srcs_libft/pf_strdup.c \
	  srcs_libft/pf_memset.c \
	  srcs_libft/pf_atoi_base.c \
	  srcs_libft/printf_color_tools.c \
	  srcs_libft/put_functions.c \
	  printf_tests/printf_tests.c \
	  printf_tests/test_tools.c

OBJ = $(SRC:.c=.o)

CFLAGS = -Wall -Wextra -Werror

INCL_DIR = ./includes

all: $(NAME)

$(NAME): $(OBJ)
	@ar -rc $(NAME) $(OBJ)
	@ranlib $(NAME)
	@echo "\033[1;34m"
	@echo "  o--o o-O-o      o--o  o--o  o-O-o o   o o-O-o o--o "
	@echo "  |      |        |   | |   |   |   |\  |   |   |    "
	@echo "  O-o    |        O--o  O-Oo    |   | \ |   |   O-o  "
	@echo "  |      |        |     |  \    |   |  \|   |   |    "
	@echo "  o      o        o     o   o o-O-o o   o   o   o    "
	@echo "             o---o                                   "
	@echo "\033[0m"

%.o : %.c
	@gcc $(CFLAGS) -I $(INCL_DIR) -o $@ -c $<

clean:
	@rm -f $(OBJ)

fclean : clean
	@rm -f $(NAME)

re : fclean all

.PHONY: all clean fclean re
