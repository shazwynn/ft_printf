/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string_options.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/28 01:28:54 by algrele           #+#    #+#             */
/*   Updated: 2018/05/19 03:15:56 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include <pf_mini_libft.h>

static int	apply_preci_field(t_format *b, char *str, int preci, int total_len)
{
	char	buff[total_len];
	int		i;

	i = 0;
	if (!b->flags[F_MINUS])
	{
		if (b->flags[F_ZERO])
			while (i < total_len - preci)
				buff[i++] = '0';
		else
			while (i < total_len - preci)
				buff[i++] = ' ';
		while (i < total_len)
			buff[i++] = *str++;
	}
	else
	{
		while (i < preci)
			buff[i++] = *str++;
		while (i < total_len)
			buff[i++] = ' ';
	}
	write(1, &buff[0], total_len);
	return (total_len);
}

int			apply_string_options(t_format *b, char *str)
{
	int		len;

	if (!str[0])
	{
		if (b->field > 0)
		{
			if (b->flags[F_ZERO])
				print_extra_spaces(b->field, '0', 1, '0');
			else
				print_extra_spaces(b->field, ' ', 1, ' ');
			return (b->field);
		}
		return (0);
	}
	len = pf_strlen(str);
	if (b->preci >= 0 && b->preci < len)
		len = b->preci;
	return (apply_preci_field(b, str, len, len > b->field ? len : b->field));
}

int			apply_wstring_options(t_format *b, wchar_t *argument)
{
	int		total_bytes;
	int		ret;

	ret = count_max_bytes(argument, 0, 0);
	if (ret == -1 && b->preci < 0)
		return (-1);
	total_bytes = ret;
	if (ret < 0)
		total_bytes = total_bytes * -1;
	if (b->preci >= 0 && b->preci < total_bytes)
		total_bytes = count_max_bytes(argument, 1, b->preci);
	if (ret < 0 && total_bytes < b->preci)
		return (-1);
	if (b->field > total_bytes && !b->flags[F_MINUS])
	{
		if (b->flags[F_ZERO])
			print_extra_spaces(b->field - total_bytes, '0', 1, '0');
		else
			print_extra_spaces(b->field - total_bytes, ' ', 1, ' ');
	}
	ret = ft_putwstr(argument, total_bytes);
	if (b->field > total_bytes && b->flags[F_MINUS])
		print_extra_spaces(b->field - total_bytes, ' ', 1, ' ');
	return (b->field > ret ? b->field : ret);
}
