/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_chars_strings.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/19 03:06:57 by algrele           #+#    #+#             */
/*   Updated: 2018/05/19 03:07:05 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include <pf_mini_libft.h>
#include <printf_tests.h>

int	print_char(t_format *b, char argument)
{
	return (apply_char_options(b, argument));
}

int	print_wchar(t_format *b, wint_t argument)
{
	return (apply_wchar_options(b, argument));
}

int	print_string(t_format *b, char *argument)
{
	if (!argument)
	{
		if (b->preci == 0)
			argument = "";
		else
			return (print_null(b));
	}
	return (apply_string_options(b, argument));
}

int	print_null(t_format *b)
{
	return (apply_string_options(b, "(null)"));
}

int	print_wstring(t_format *b, wchar_t *argument)
{
	if (!argument)
	{
		if (b->preci == 0)
			return (apply_string_options(b, ""));
		return (print_null(b));
	}
	return (apply_wstring_options(b, argument));
}
