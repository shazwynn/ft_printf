/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_unicode2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/19 03:10:43 by algrele           #+#    #+#             */
/*   Updated: 2018/05/19 03:15:43 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include <pf_mini_libft.h>

int		is_next_unicode_conv(const char *format)
{
	int	i;

	i = 0;
	while (format[i] && format[i] != '%')
		i++;
	if (format[i] && format[i] == '%')
		i++;
	while (format[i] && c_in_macro(format[i], FLAGS))
		i++;
	while (format[i] && c_in_macro(format[i], NUMBERS))
		i++;
	if (format[i] && format[i] == '.')
		i++;
	while (format[i] && c_in_macro(format[i], NUMBERS))
		i++;
	if (format[i] && c_in_macro(format[i], MODIFIERS))
		i++;
	if (i > 0 && format[i] && (format[i] == 'h' || format[i] == 'l') &&
			format[i] == format[i - 1])
		i++;
	if (format[i] && (format[i] == 'C' || (format[i] == 'c' && i > 1 &&
					format[i - 1] == 'l')))
		return (1);
	return (0);
}

int		check_error_wide_c(wint_t wc)
{
	if (wc < 0)
		return (0);
	if (wc >= 256 && MB_CUR_MAX == 1)
		return (0);
	if (wc > 55295 && wc < 57344)
		return (0);
	if (wc > 1114111)
		return (0);
	return (1);
}

/*
** might have broken a bunch of extra tests, see main_test
** limit values for UTF-8 : U+0000 to U+D7FF and U+E000 to U+10FFFF
*/

int		count_wchar_bytes(wint_t wc)
{
	if (wc < 0)
		return (-1);
	if (wc >= 256 && MB_CUR_MAX == 1)
		return (-1);
	if (MB_CUR_MAX == 1)
		return (1);
	if (wc > 55295 && wc < 57344)
		return (-1);
	if (wc <= 127)
		return (1);
	else if (wc <= 2047)
		return (2);
	else if (wc <= 65535)
		return (3);
	else if (wc <= 1114111)
		return (4);
	return (-1);
}

/*
** return total_bytes found, in negative form if an error was found, up to
** the error.
*/

int		count_max_bytes(wchar_t *s, int toggle, int max_bytes)
{
	int		total_bytes;
	wint_t	c;
	int		count;

	total_bytes = 0;
	while (s && s[0])
	{
		c = *s++;
		count = count_wchar_bytes(c);
		if (count == 1 && (!toggle || max_bytes >= 1))
			count = 1;
		else if (count == 2 && (!toggle || max_bytes >= 2))
			count = 2;
		else if (count == 3 && (!toggle || max_bytes >= 3))
			count = 3;
		else if (count == 4 && (!toggle || max_bytes >= 4))
			count = 4;
		else if (toggle)
			return (total_bytes);
		else
			return (total_bytes * -1);
		total_bytes += count;
		max_bytes -= count;
	}
	return (total_bytes);
}
