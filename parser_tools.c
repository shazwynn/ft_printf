/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser_tools.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/27 22:25:13 by algrele           #+#    #+#             */
/*   Updated: 2018/05/18 16:16:13 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include <pf_mini_libft.h>

/*
** clearer representations of types, especially 'wide' / 'long'.
*/

int			clean_type(t_format *bubble)
{
	if (bubble->type == 'i')
		bubble->type = 'd';
	if (c_in_macro(bubble->type, "SDOUC"))
	{
		bubble->modi = 3;
		bubble->type = bubble->type + 32;
	}
	return (1);
}

/*
** clears up incompatible flags.
** '-' (left adjust) overrides a '0' (right adjust with zeroes)
** '0' flag ignored if a precision is set for number conversions
*/

int			clean_flags(t_format *b)
{
	if (b->flags[F_ZERO])
	{
		if (b->flags[F_MINUS])
			b->flags[F_ZERO] = 0;
		else if (b->preci >= 0 && c_in_macro(b->type, NP_CONVERSIONS))
			b->flags[F_ZERO] = 0;
	}
	if (b->flags[F_PLUS] && b->flags[F_SPACE])
		b->flags[F_SPACE] = 0;
	return (1);
}

/*
** activated flags have value of 1, else 0.
*/

void		check_cut_flags(t_format *bubble, char *flags, int stop)
{
	int i;

	i = 0;
	while (flags[i] && i <= stop)
	{
		if (flags[i] == '#')
			bubble->flags[F_SHARP] = 1;
		else if (flags[i] == '0')
			bubble->flags[F_ZERO] = 1;
		else if (flags[i] == '-')
			bubble->flags[F_MINUS] = 1;
		else if (flags[i] == '+')
			bubble->flags[F_PLUS] = 1;
		else if (flags[i] == ' ')
			bubble->flags[F_SPACE] = 1;
		i++;
	}
}

/*
** assigns value to bubble->modi to reflect type conversion modifier
*/

void		assign_modifier(char first, char second, int *modifier)
{
	if (first == 'h' && second == '.')
		*modifier = 1;
	else if (first == 'h' && second == 'h')
		*modifier = 2;
	else if (first == 'l' && second == '.')
		*modifier = 3;
	else if (first == 'l' && second == 'l')
		*modifier = 4;
	else if (first == 'j' && second == '.')
		*modifier = 5;
	else if (first == 'z' && second == '.')
		*modifier = 6;
}

/*
** create, allocate memory and initialize t_format struct.
*/

t_format	*init_format_struct(void)
{
	t_format	*bubble;

	if (!(bubble = (t_format *)malloc(sizeof(t_format))))
		return (NULL);
	bubble->flags[0] = 0;
	bubble->flags[1] = 0;
	bubble->flags[2] = 0;
	bubble->flags[3] = 0;
	bubble->flags[4] = 0;
	bubble->type = 0;
	bubble->field = 0;
	bubble->preci = -1;
	bubble->modi = 0;
	return (bubble);
}
