/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_tools.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/03 17:30:03 by algrele           #+#    #+#             */
/*   Updated: 2018/05/18 16:19:48 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <pf_mini_libft.h>
#include <ft_printf.h>
#include <printf_tests.h>

void	test_return(int ret)
{
	pf_putstr("-> (");
	pf_putnbr(ret);
	pf_putstr(")\n");
}

void	pf_putstr_no_sp(char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		if (str[i] == ' ')
			pf_putstr("\e[38;4;40m \e[0m");
		else if (str[i] == '\n')
			pf_putstr_c("\\n", GREEN);
		else
			pf_putchar(str[i]);
		i++;
	}
}

void	ft_fill(char *str, int start, int len, char c)
{
	int	i;

	i = 0;
	if (str)
		str = str + start;
	while (i < len)
	{
		str[i] = c;
		i++;
	}
}

void	test_message(char *s, char *color)
{
	int len;
	int	i;

	i = 0;
	len = pf_strlen(s) + 10;
	pf_putchar('\n');
	while (i < len)
	{
		pf_putchar_c('~', color);
		i++;
	}
	pf_putstr_c("\n~~~~ ", color);
	pf_putstr_c(s, color);
	pf_putendl_c(" ~~~~", color);
	i = 0;
	while (i < len)
	{
		pf_putchar_c('~', color);
		i++;
	}
	pf_putchar('\n');
}
