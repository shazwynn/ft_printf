/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   instructions_parser.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/28 02:06:16 by algrele           #+#    #+#             */
/*   Updated: 2018/05/03 17:53:32 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include <pf_mini_libft.h>
#include <printf_tests.h>

/*
** adds flags to struct and field length. field length will never be <= 0
** because a '-' or '0' would be interpreted as a flag.
*/

static void	parse_flags_field(t_format *bubble, char *format, int *skip)
{
	int		j;
	int		i;

	j = 0;
	i = *skip;
	while (format[i++] && c_in_macro(format[i], FLAGS))
		j++;
	check_cut_flags(bubble, format, j);
	j = 0;
	while (format[i] && c_in_macro(format[i], NUMBERS))
	{
		bubble->field = (10 * bubble->field) + format[i] - '0';
		i++;
		j++;
	}
	*skip = i;
}

/*
** adds precision and modifiers to struct. if there is no number after the
** precision '.', it defaults to zero.
*/

static void	parse_preci_modi(t_format *bubble, char *f, int *skip)
{
	int		i;

	i = *skip;
	if (f[i] && f[i] == '.' && i++)
	{
		bubble->preci = 0;
		while (f[i] && c_in_macro(f[i], NUMBERS))
			bubble->preci = (10 * bubble->preci) + f[i++] - '0';
	}
	if (f[i] && c_in_macro(f[i], MODIFIERS))
	{
		if (f[i + 1] && (f[i + 1] == 'l' || f[i + 1] == 'h') && i++)
			assign_modifier(f[i - 1], f[i], &bubble->modi);
		else
			assign_modifier(f[i], '.', &bubble->modi);
		i++;
	}
	*skip = i;
}

/*
** format[0] is a '%' in format string, parse what is afterwards and store
** results in t_format *bubble struct.
** %[flags][field](.)[precision][modifier]type.
** set skip to the number of chars for the current conversion.
*/

t_format	*parse_instructions(t_format *b, char *format, int *skip)
{
	int		i;

	i = 0;
	if (format[i + 1])
	{
		parse_flags_field(b, format, &i);
		parse_preci_modi(b, format, &i);
		b->type = format[i];
		clean_type(b);
		clean_flags(b);
		*skip = i + 1;
		return (b);
	}
	*skip = 1;
	return (b);
}
