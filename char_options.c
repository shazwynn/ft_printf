/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   char_options.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: algrele <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/28 00:31:26 by algrele           #+#    #+#             */
/*   Updated: 2018/05/18 19:23:38 by algrele          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>
#include <pf_mini_libft.h>
#include <printf_tests.h>

void	print_extra_spaces(int field, char c, int pos, char extra)
{
	char	buff[field];
	int		i;

	if (pos)
	{
		i = 0;
		while (i < field - 1)
			buff[i++] = extra;
		buff[i] = c;
	}
	else
	{
		i = 1;
		buff[0] = c;
		while (i < field)
			buff[i++] = extra;
	}
	write(1, &buff[0], field);
}

int		apply_char_options(t_format *b, char c)
{
	if (b->field > 1)
	{
		if (b->flags[F_MINUS])
			print_extra_spaces(b->field, c, 0, ' ');
		else if (b->flags[F_ZERO])
			print_extra_spaces(b->field, c, 1, '0');
		else
			print_extra_spaces(b->field, c, 1, ' ');
	}
	else
		write(1, &c, 1);
	return (b->field > 1 ? b->field : 1);
}

int		apply_wchar_options(t_format *b, wint_t wc)
{
	int bytes;
	int	written;

	if ((bytes = count_wchar_bytes(wc)) == -1)
		return (-1);
	if (!wc)
		bytes = 1;
	if (b->field > bytes)
	{
		if (!b->flags[F_MINUS])
		{
			if (b->flags[F_ZERO])
				print_extra_spaces(b->field - bytes, '0', 1, '0');
			else
				print_extra_spaces(b->field - bytes, ' ', 1, ' ');
		}
		written = ft_putwchar(wc, 4);
		if (b->flags[F_MINUS])
			print_extra_spaces(b->field - bytes, ' ', 1, ' ');
	}
	else
		written = ft_putwchar(wc, 4);
	return (b->field > written ? b->field : written);
}
